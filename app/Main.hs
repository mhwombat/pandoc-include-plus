{-|
Module      : Main
Description : Extends the Markdown syntax to support include files.
Copyright   : (c) Amy de Buitléir, 2023
License     : BSD--3
Maintainer  : amy@nualeargais.ie
Stability   : experimental
Portability : POSIX

See <https://codeberg.org/mhwombat/pandoc-include-plus> for information
on how to use this filter.
-}
import System.Directory (getCurrentDirectory)
import Text.Pandoc.Filters.Include (transform)
import Text.Pandoc.JSON            (toJSONFilter)

main :: IO ()
main = do
  d <- getCurrentDirectory
  toJSONFilter (transform d)
