# pandoc-include-plus

A pandoc filter which supports "include" files.
*At the moment, only Markdown files can be included. However, I expect soon to be able to support all filetypes that Pandoc supports.*
If included files are not found, an error occurs.
Included files can include other files, recursively.
Paths to images are adjusted as needed to ensure that everything "just works".

To include files, list them in a code block with the `include` class.
Here is an example in Markdown syntax.

    ```include
    file1.md
    file2.md
    file3.md
    ```

You can also promote or demote headings in included files using the `level` keyword.
In the example below, and level-1 headings in the file `a.md` will become level-2 headings,
level2-headings become level-3 headings, and so on.

    ```{.include level="1"}
    a.md
    ```

To demote headings, set `level` to a negative value.
A heading that would be shifted below level 1 becomes a regular paragraph.
