# Revision history for pandoc-include-ln

- 0.1.2 Fixed incorrect cabal file.
- 0.1.1 Automatically adjust paths to images in included files.
- 0.1.0 First version. Released on an unsuspecting world.
