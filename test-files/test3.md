```
>>> BEGIN test3.md
```

# This was a level-1 heading; it should now be normal text

Some normal text

## This was a level-2 heading; it should now be level-1

more normal text

### This was a level-3 heading; it should now be level-2

```
>>> END test3.md
```
