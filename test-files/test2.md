```
>>> BEGIN test2.md
```

# This was a level-1 heading; it should now be level-2

Some normal text

## This was a level-2 heading; it should now be level-3

more normal text

### This was a level-3 heading; it should now be level-4

```
>>> END test2.md
```
