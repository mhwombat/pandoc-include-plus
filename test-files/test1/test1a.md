```
>>> BEGIN test1a.md
```

Contents of a nested include file.

Here's an image:

![](test.png)

```
>>> END test1a.md
```
