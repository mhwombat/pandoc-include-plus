# TEST

    >>> BEGIN test1.md

Contents of the first include file.

    >>> BEGIN test1a.md

Contents of a nested include file.

Here's an image:

![](test1/test.png)

    >>> END test1a.md

    >>> END test1.md

    >>> BEGIN test2.md

## This was a level-1 heading; it should now be level-2

Some normal text

### This was a level-2 heading; it should now be level-3

more normal text

#### This was a level-3 heading; it should now be level-4

    >>> END test2.md

    >>> BEGIN test3.md

This was a level-1 heading; it should now be normal text

Some normal text

# This was a level-2 heading; it should now be level-1

more normal text

## This was a level-3 heading; it should now be level-2

    >>> END test3.md
